
module.exports = {
  // Definir redes a utilizar
  networks : {
    // Red de desarrollo de GANACHE
    development : {
      host : 'localhost',
      port : 7545,
      network_id : '*', // matchear cualquier red contra la que se lancen los scripts
      gas : 5000000 // limite de gas por transaccion
    }

  }
}
