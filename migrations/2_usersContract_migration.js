// Indicar a truffle que utilice el artefacto del Smart Contract UsersContract.sol
let UsersContract = artifacts.require('./UsersContract.sol');

// Configurar el deployer para que despliegue UsersContract
module.exports = function(deployer) {
    deployer.deploy(UsersContract);
}