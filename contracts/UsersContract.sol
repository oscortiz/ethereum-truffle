pragma solidity ^0.4.24;

// Contrato
contract UsersContract {

    // Estructura de definicion de usuario
    struct User {
        string name;
        string surname;
    }

    // Mapping para representar la coleccion de usuarios
    // Para cada direccion de ethereum un usuario
    mapping(address => User) private users;
    // Mapping para comprobar si un usario esta registrado
    mapping(address => bool) private joinedUsers;
    // Array para conocer el numero de usuarios registrados
    address[] total;

    // Evento para informar de lo sucedido
    event onUserJoined(address, string);

    // Registrar un usario pasando a formar parte del mapping de usuarios
    function join(string _name, string _surname) public {
        // Requerir que el usuario no este registrado
        require(!joinedUsers[msg.sender], "User Joined");
        // El addres del mapping sera la direccion de la persona
        // que esta ejecutando la transaccion
        // El usuario debera ser de tipo storage para que los cambios
        // persistan dentro del contrato
        User storage user = users[msg.sender];
        user.name = _name;
        user.surname = _surname;
        // Añadir el usuario como ya resgistrado
        joinedUsers[msg.sender] = true;
        total.push(msg.sender);
        // Disparar evento
        // abi.encodePacked -> utilidad para concatenar strings
        emit onUserJoined(msg.sender, string(abi.encodePacked(_name, " ", _surname)));
    }

    // Recuperar nombre y apellido de un usario registrado a partir de su direccion de ethereum
    // Marcamos como view para indicar que sera solo de visualizacion -> NO GAS (gratuita)
    function getUser(address _addr) public view returns (string, string) {
        // Requerir que el usuario este registrado
        require(joinedUsers[msg.sender], "User Not Joined");
        // El usuario debera ser de tipo memory para que los cambios
        // no persistan dentro del contrato -> sera de solo lectura
        User memory user = users[_addr];
        return (user.name, user.surname);
    }

    // Verificar si un usario ya esta registrado en el contrato
    function userJoined(address _addr) private view returns (bool) {
        return joinedUsers[_addr];
    }

    // Recuperar el total de usarios registrados
    function totalUsers() public view returns (uint) {
        return total.length;
    }
}